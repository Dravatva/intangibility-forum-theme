<!DOCTYPE html>
<html>
<head>
   {asset name="Head"}
</head>
<body id="{$BodyID}" class="{$BodyClass}">
   <div id="Frame">
      <div class="Head" id="Head">
         <div class="Row">
            <strong class="SiteTitle"><a href="{link path="/"}">{literal}
	       <svg id="logo" viewBox="0 0 426.03 92.114" width="30%" height="30%">
		  <g transform="translate(-5.3333333,-1.3333332)">
		     <path class="path" d="M63.5,44.667,97.333,30s-13.667,46.043-33.333,53.167c-19.178,6.9468-58.167-19-58.167-19s38.211,12.565,52.167,2.1667c5.9751-4.4519,5.5-21.667,5.5-21.667z"/>
		     <path class="path" d="m76.333,31.667c-4.3163,0.04546-9.2007-2.9097-11-6.8333-2.2501-4.9067,2.5-16,2.5-16,0.611,4.499-0.111,6.499,3.167,8.166,2.167-5.5-0.333-9.3333,3-15.167,1.889,5.8889,5.278,11.945,6,19.667,0.61111-3.0556,3.7222-9.1111,2.6667-12.333,0,0,6.1054,10.623,4.1667,15.667-1.4982,3.8979-6.3243,6.7894-10.5,6.8333z"/>
		     <text class="text" style="font-size:56px;font-family:Hiragino Sans GB;letter-spacing:0px;font-style:normal;word-spacing:0px;font-stretch:normal;font-variant:normal;font-weight:bold;line-height:125%;" xml:space="preserve" y="77.743484" x="95.695122"><tspan x="95.695122" y="77.743484">ntangibility</tspan></text>
		  </g>
	       </svg>{/literal}</a></strong>
            <div class="SiteSearch">{searchbox}</div>
            <ul class="SiteMenu">
               <!-- {dashboard_link} -->
               <a title="Home" href="{link path="categories"}">
	       <svg class="Icon" ViewBox="0 0 64 64" width="20px" height="20px">
		  <path class="path" style="stroke-linejoin:round;stroke-linecap:round;stroke-miterlimit:4;stroke-width:4;fill:none;" d="m47.136,28.818,0,30.273-30.273,0,0-30.273"/>
		  <path class="path" style="stroke-linejoin:round;stroke-linecap:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-width:4;fill:none;" d="M7.3636,28.909,32,5.6364,56.636,27.636"/>
	       </svg>
	       </a>
               <a title="Recent Discussions" href="{link path="discussions"}">
	       <svg class="Icon" ViewBox="0 0 64 64" width="20px" height="20px">
		  <g transform="translate(-0.12444493,-0.31360429)">
		     <path class="path" d="m60,27.909c0,9.5897-12.088,17.364-27,17.364-4.4978,0-8.7387-0.70728-12.469-1.9583-1.807-0.607-14.586,11.568-16.124,10.725-1.5002-0.822,8.2325-14.657,7.0425-15.669-3.4205-2.91-5.449-6.534-5.449-10.462,0-9.59,12.088-17.364,27-17.364s27,7.774,27,17.364z" style="stroke-linejoin:round;stroke-linecap:round;stroke-miterlimit:4;stroke-width:4;fill:none;"/>
		     <path class="path" id="path4428" d="m15.455,22.364,35.636,0" style="stroke-linejoin:miter;stroke-linecap:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-width:2;fill:none;"/>
		     <use id="use4463" xlink:href="#path4428" transform="translate(-4.5454547e-8,5.6363671)" height="64" width="64" y="0" x="0"/>
		     <use xlink:href="#use4463" transform="translate(-4.5454547e-8,5.6363601)" height="64" width="64" y="0" x="0"/>
		  </g>
	       </svg>
	       </a>
	       <a title="Mark all as viewed" href="{link path="discussions/markallviewed"}">
	       <svg class="Icon" ViewBox="0 0 64 64" width="20px" height="20px">
		  <path class= "path" style="stroke-linejoin:miter;stroke-linecap:butt;stroke-miterlimit:4;stroke-dasharray:none;stroke-width:4;fill:none;" d="m3.2313,33.273s16.872-18.804,28.185-18.909c11.638-0.1084,29.352,18.909,29.352,18.909s-18.151,16.461-29.352,16.364c-10.863-0.095-28.185-16.364-28.185-16.364z"/>
		  <path class="path" style="stroke-linejoin:round;stroke-linecap:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-width:3.70454546;fill:none;" d="m55.636,21a12.818,12.818,0,1,1,-25.636,0,12.818,12.818,0,1,1,25.636,0z" transform="matrix(1.0797546,0,0,1.0797546,-14.233127,9.3251534)"/>
	       </svg>
	       </a>
               <!-- {inbox_link} -->
               {custom_menu}
               <!-- {profile_link} -->
	       <a title="Profile" href="{link path="profile"}">
	       <svg class="Icon" ViewBox="0 0 64 64" width="20px" height="20px">
		  <path class="path" style="stroke-linejoin:round;stroke-linecap:butt;stroke-miterlimit:4;stroke-dasharray:none;stroke-width:4;fill:none;" d="M24.406,28.438l-11.937,33.374h39.062l-11.937-33.374c-2.2,1.386-4.802,2.187-7.594,2.187s-5.394-0.801-7.594-2.187z"/>
		  <path class="path" style="stroke-linejoin:round;stroke-linecap:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-width:4;fill:none;" d="m49.091,19.545a11.182,11.182,0,1,1,-22.364,0,11.182,11.182,0,1,1,22.364,0z" transform="translate(-5.9090919,-4.9999994)"/>
	       </svg>
	       </a>
               <!-- {signinout_link} NOT WORKING YET
	       <a href="{link path="signinout"}">
	       {literal}
	       <svg class="Icon" ViewBox="0 0 64 64" width="20px" height="20px">
		  <path class="path" style="stroke-linejoin:round;stroke-linecap:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-width:4;fill:none;" d="m15.469,11.818c-6.6774,5.0265-11,12.998-11,22,0,15.213,12.318,27.531,27.531,27.531s27.531-12.318,27.531-27.531c0-9.0015-4.3226-16.974-11-22"/>
		  <path class="path" style="stroke-linejoin:round;stroke-linecap:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-width:4;fill:none;" d="M32,2.9091,32,34.909"/>
	       </svg>
	       {/literal}
	       </a> -->
            </ul>
         </div>
      </div>
      <div id="Body">
         <div class="Row">
            <div class="BreadcrumbsWrapper">{breadcrumbs}</div>
            <div class="Column PanelColumn" id="Panel">
               {module name="MeModule"}
               {asset name="Panel"}
            </div>
            <div class="Column ContentColumn" id="Content">{asset name="Content"}</div>
         </div>
      </div>
      <div id="Foot">
         <div class="Row">
            <a href="{vanillaurl}" class="PoweredByVanilla" title="Community Software by Vanilla Forums">Powered by Vanilla</a>
            {asset name="Foot"}
         </div>
      </div>
   </div>
   {event name="AfterBody"}
</body>
</html>
