Intangibility Forum Theme
=========================

Theme for Intangibility Forums.

Developed by Dravatva.

Branch "master" is tested stable, branch "dev" hasn't and likely doesn't work properly.

Installation
------------

Download the repository (either with `git clone` or by downloading it as a zip file (see link on right sidebar). Place contents in a folder named "intang" (it can have another name, but in that case about.php will have to be edited to reflect that) in the "themes" directory in the vanilla install and activate from the dashboard like any theme. When uploading a new version remember to replace all files with their new versions (`\cp -rf` in bash, for example).

You will also need to add the following to config.php:

* `$Configuration['Plugins']['AllViewed']['ShowInMenu'] = FALSE;`
* `$Configuration['Garden']['Thumbnail']['Size'] = 90;`

Known Issues
------------
